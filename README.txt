A talk given a Mines LUG in 2019 by Sam Sartor, Jesus Nunez, et al. It explains
the history of desktop graphical environments in Linux operating systems, and
how you can customize them to your liking!
